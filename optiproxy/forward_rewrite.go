package optiproxy

const (
	XOpenstackRequestID = "X-Openstack-Request-Id"
	XTimestamp          = "X-Timestamp"
	XTransID            = "X-Trans-Id"
)

var OpenStackHeader = []string{
	XOpenstackRequestID,
	XTimestamp,
	XTransID,
}
