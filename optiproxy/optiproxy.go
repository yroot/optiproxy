package optiproxy

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/ncw/swift"
	"github.com/vulcand/oxy/forward"
	"github.com/vulcand/oxy/utils"
	"gitlab.com/yroot/optiproxy/optiproxy/config"
)

// OptiProxy Model
type OptiProxy struct {
	Addr   string
	Swift  *swift.Connection
	Config config.Config
}

// New creates a new proxy instance
func New() *OptiProxy {
	filename, err := filepath.Abs("./optiproxy.yaml")
	if err != nil {
		log.Fatalln(err)
	}
	c := config.ReadConfig(filename)
	log.Println("Host:", c.Host)
	for zname, zone := range c.Zones {
		log.Println("Zone:", zname, "to", zone.OriginURL)
	}
	return &OptiProxy{Addr: ":8080", Config: c}
}

// ListenAndServe starts the proxy
func (optiproxy *OptiProxy) ListenAndServe() {
	r := mux.NewRouter()

	fwd, _ := forward.New(
		forward.ResponseModifier(func(resp *http.Response) error {
			utils.RemoveHeaders(resp.Header, OpenStackHeader...)
			resp.Header.Set("Vary", "Accept-Encoding")

			age := (time.Hour * 24 * 30) / time.Second
			resp.Header.Set("Cache-Control", fmt.Sprintf("max-age=%d, public, must-revalidate, proxy-revalidate", age))
            resp.Header.Set("Expires", time.Now().AddDate(0, 0, 30).Format(http.TimeFormat))

			return nil
		}),
	)

	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hi!\n"))
	})

	r.Host("{zone}." + optiproxy.Config.Host).
		Path("/{path:[a-zA-Z0-9\\/\\-\\_\\.\\%\\+]*}").
		HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		zoneName := vars["zone"]
		path := vars["path"]
		zone := optiproxy.Config.Zones[zoneName]
		rawurl := zone.OriginURL
		rawurl = strings.TrimRight(rawurl, "/") + "/"
		req.URL, _ = url.Parse(rawurl + path)
		req.RequestURI = rawurl + path

		log.Println(req.URL)
		fwd.ServeHTTP(w, req)
	})
	
	loggedRouter := handlers.LoggingHandler(os.Stdout, r)

	s := &http.Server{
		Addr:         optiproxy.Addr,
		Handler:      loggedRouter,
		WriteTimeout: 30 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	s.ListenAndServe()

	os.Exit(0)
}

//todo: Implement optim
func (optiproxy *OptiProxy) x(w http.ResponseWriter, req *http.Request) {
	log.Println(req.URL)
	out, _ := os.Create("output.jpg")
	defer out.Close()
	resp, err := http.Get(req.URL.String())
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()
	io.Copy(out, resp.Body)

	// List all the containers
	container, _, err := optiproxy.Swift.Container("local")
	fmt.Println(container)
	//	opti.Swift.ObjectPutBytes(container.Name, "test.jpg", resp.Body
	fmt.Println(err)

}
