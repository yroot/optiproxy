package config

import (
	"path/filepath"
	"testing"
)

func TestReadConfig(t *testing.T) {

	filename, _ := filepath.Abs("./test/test.yaml")
	c := ReadConfig(filename)

	var zone = "test"
	var originURL = "https://test.com"
	if c.Zones[zone].OriginURL != originURL {
		t.Errorf("%s Zone has wrong OriginURL %s != %s", zone, c.Zones[zone].OriginURL, originURL)
	}
	zone = "test1"
	originURL = "https://test1.com"
	if c.Zones[zone].OriginURL != originURL {
		t.Errorf("%s Zone has wrong OriginURL %s != %s", zone, c.Zones[zone].OriginURL, originURL)
	}
	zone = "test2"
	if c.Zones[zone].OriginURL != "" {
		t.Errorf("%s Zone shouldn't have a OriginURL (%s)", zone, c.Zones[zone].OriginURL)
	}
}
