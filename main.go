package main

import (
	"gitlab.com/yroot/optiproxy/optiproxy"
)

func main() {
	proxy := optiproxy.New()
	proxy.ListenAndServe()
}
