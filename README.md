# OptiProxy

Proxy server to optimize images written in go.

[![pipeline status](https://gitlab.com/yroot/optiproxy/badges/master/pipeline.svg)](https://gitlab.com/yroot/optiproxy/commits/master)
[![coverage report](https://gitlab.com/yroot/optiproxy/badges/master/coverage.svg)](https://gitlab.com/yroot/optiproxy/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/yroot/optiproxy)](https://goreportcard.com/report/gitlab.com/yroot/optiproxy)
[![GoDoc](https://godoc.org/gitlab.com/yroot/optiproxy?status.svg)](https://godoc.org/gitlab.com/yroot/optiproxy)