# This file is a template, and might need editing before it works on your project.
FROM golang:alpine AS builder

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates git

WORKDIR /go/src/app

COPY . /go/src/app

RUN go get -u github.com/golang/dep/...
RUN dep ensure

RUN CGO_ENABLED=0 GOOS=linux go build -v -a -installsuffix cgo -o app .

FROM scratch

# Since we started from scratch, we'll copy the SSL root certificates from the builder
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

WORKDIR /data/app

COPY --from=builder /go/src/app/app .
CMD ["./app"]
